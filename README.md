# Clone du git
Clone du git avec le submodule :
```
git clone --recursive https://gitlab.com/lamisedaxeh/config
```

Update/DL les submodules hors du clone :
```
git submodule update --init --recursive
```

# UEFI Install Arch
## Basic setup
### Création des partitions

 - Boot on UEFI 

Charger le clavier azerty
```loadkeys fr```

Trouver le bon disque :
```lsblk``` 
Puis partitioner ce disk avec :
```cgdisk``` 
Pour avoir trois partitions : 
 - swap (mini la ram size)
 - /   
 - efi (1go)

Trouver les noms des partitions avec :
```
lsblk 
```
Formater les partitions et le swap : 
```
mkfs.ext4 /dev/sdd1
mkfs.fat -F32 /dev/sdd3 
mkswap /dev/sdd2
swapon /dev/sdd2
``` 

Monter les partitions :
``` 
mount /dev/sdd1 /mnt
mkdir /mnt/{boot,boot/efi,home}
mount /dev/sdd3 /mnt/boot/efi
``` 

### Install base

Configurer les mirrors pacman et le parallel download:
``` 
vim /etc/pacman.d/mirrorlist
vim /etc/pacman.conf
``` 


Install base and usefull package :
```
pacstrap /mnt base base-devel linux linux-firmware
pacstrap /mnt zip unzip p7zip vim mc alsa-utils syslog-ng mtools dosfstools lsb-release ntfs-3g exfat-utils bash-completion
``` 

Generate fstab : 
```
genfstab -U /mnt >> /mnt/etc/fstab
```

## Chroot !
Enter Chroot
```arch-chroot /mnt```
### Time
Set timezone :
```
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime 
hwclock --systohc
```
### Language
Edit /etc/locale.gen and uncomment :
```
en\_US.UTF-8 UTF-8 
fr\_FR.UTF-8 UTF-8
```

Generate locale :
```
locale-gen
```

add to file /etc/locale.conf :
```
LANG=fr\_FR.UTF-8
LC\_COLLATE=C
```

### keyboard
  
to setup keyboard add this to /etc/vconsole.conf :
```
KEYMAP=fr-latin9
FONT=eurlatgr
```

### Network
Create hostname file /etc/hostname and fill it:
```lami-arch-tour ```

Add hosts in /etc/hosts :
```
127.0.0.1	localhost
::1		localhost
```


### initramfs & root password
run :
```
mkinitcpio -P
```
And change the root password :
```
passwd
```


### Boot

```
pacman -S grub os-prober efibootmgr
```

Setup grub config :
```
mount | grep efivars &> /dev/null || mount -t efivarfs efivarfs /sys/firmware/efi/efivars
grub-install --target=x86\_64-efi --efi-directory=/boot/efi --bootloader-id=arch\_grub --recheck
```

Pour les machine dans virutal box :
```
mkdir /boot/efi/EFI/boot
cp /boot/efi/EFI/arch\_grub/grubx64.efi /boot/efi/EFI/boot/bootx64.efi
```

Create grub config file
```
grub-mkconfig -o /boot/grub/grub.cfg
```

### Setup network
```
pacman -Syy networkmanager
systemctl enable NetworkManager
```


# TODO
```
pacman -S ntp 
pacman -S gst-plugins-{base,good,bad,ugly} gst-libav
pacman -S xorg-{server,xinit,apps}  xdg-user-dirs
pacman -S nvidia
pacman -S gimp gimp-help-fr man-db texinfo man-pages
pacman -S libreoffice-still-fr hunspell-fr
pacman -S firefox-i18n-fr
```

Virtualbox :
```
pacman -S xf86-video-vesa
pacman -S virtualbox-guest-utils
```

Create a user :
```
useradd -m -g wheel -c lamisedaxeh -s /bin/bash lamisedaxeh
passwd lamisedaxeh
```

```
pacman -S sudo vi
#Uncomment to allow members of group wheel to execute any command in /etc/sudoers
pacman -S gnome gnome-extra system-config-printer vlc
pacman -S keepassxc
```




```
sudo pacman -S git
git clone https://aur.archlinux.org/yay
cd yay
makepkg -sri
```



```
systemctl enable syslog-ng@default → *gestion des fichiers d’enregistrement d’activité*
systemctl enable ntpd → *pour synchroniser l’heure en réseau.*
```
```
```
```
```


# NEED
> LSD #(ls deluxe)




