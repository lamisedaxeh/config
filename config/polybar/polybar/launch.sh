#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch bar1 and bar2
echo "---" | tee -a /tmp/polybarDefault.log # /tmp/polybar2.log
polybar default >> /tmp/polybarDefault.log 2>&1 &
#polybar bar2 >>/tmp/polybar2.log 2>&1 &

echo "Bars launched..."
