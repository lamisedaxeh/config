# .dotfile managed with stow


## Install the .dotfile

To install all the .dotfile run this command in this folder (slash is needed for picking directory only) :

```
stow -v */ -t ~
```

Simulate and show the modification :
```
stow -nv */ -t ~
```

How does stow works ?
![](stow.webp)

## Add .dotfile

Create a folder with an explicit name for putting your .dotfile, exemeple a "zsh" folder for zsh files. 
The all the file in this folder will be unpack to the target directory ~ in the previous exemple. 

![](unpacking.png)

## Other :

Other usefull args : 
```
-R : For remove and recreate the link 
-D : Removing the link
```
