# How to add a module 

## Update
Update a submodule :
```sh
git submodule update dotfiles/zsh/.oh-my-zsh/plugins/zsh-autosuggestions
```

## Add module
Add the submodule repository :
```sh
git submodule add -b master https://github.com/zsh-users/zsh-autosuggestions
```

Add the module name in the plugins part in the .zshrc file :
```
plugins=(
  git
  extract
  zsh-syntax-highlighting
  zsh-autosuggestions
  colored-man-pages
  catimg
)
```
